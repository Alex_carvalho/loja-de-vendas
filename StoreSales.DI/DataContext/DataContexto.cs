﻿using System.Data.Entity;
using StoreSales.DI.Models;

namespace StoreSales.DI.DataContext
{
    public class DataContexto : DbContext
    {
        //public DbSet<Entradas> Entradas { get; set; }
        //public DbSet<Orcamento> Orcamentos { get; set; }
        //public DbSet<Parametros> Parametros { get; set; }
        //public DbSet<Preco> Precos { get; set; }
        //public DbSet<Produtos> Produtos { get; set; }
        //public DbSet<Saidas> Saidas { get; set; }
        //public DbSet<SaidasRetorno> SaidasRetornos { get; set; }
        //public DbSet<TipoEntrada> TipoEntradas { get; set; }
        //public DbSet<TipoVenda> TipoVendas { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
        
        public DataContexto()
            : base("strString") { }

        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<Usuario>().ToTable("Usuario");

        //    modelBuilder.Entity<Usuario>()
        //        .HasKey(c => c.Id).HasKey(c => c.CodUsuario);

        //    base.OnModelCreating(modelBuilder);
        //}
    }
}
