﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StoreSales.DI.Models
{
    public class Usuario
    {
        public int Id { get; set; }
        public String CodUsuario { get; set; }
        public String Nome { get; set; }
        public String Tsenha { get; set; }
        public String Status { get; set; }
        public String Nivel { get; set; }

    }
}
