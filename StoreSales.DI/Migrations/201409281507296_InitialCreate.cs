using System.Data.Entity.Migrations;

namespace StoreSales.DI.Migrations
{   
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Usuarios",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CodUsuario = c.String(),
                        Nome = c.String(),
                        Tsenha = c.String(),
                        Status = c.String(),
                        Nivel = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Usuarios");
        }
    }
}
