﻿using System.Collections.Generic;
using System.Linq;
using StoreSales.DI.Models;
using StoreSales.DI.Repositorio;

namespace StoreSales.DI.Factory
{
    public class UsuariosFactory
    {
        public Usuario VerificaLogin(string pNome, string pSenha)
        {
            var rep = new RepositoryFactory<Usuario>().CriarRepositorio();

            Usuario usuario =
                rep.Query(c => c.Nome.Trim() == pNome && c.Tsenha == pSenha).FirstOrDefault();

            return usuario;
        }

        public List<Usuario> RetornaSeExisteUsuario()
        {
            var rep = new RepositoryFactory<Usuario>().CriarRepositorio();

            List<Usuario> usuario = rep.GetAll().ToList();

            return usuario;
        }
    }
}