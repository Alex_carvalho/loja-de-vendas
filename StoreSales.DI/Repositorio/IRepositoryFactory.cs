﻿using StoreSales.DI.Repositorio.Base;

namespace StoreSales.DI.Repositorio
{
    public interface IRepositoryFactory<T> where T : class
    {
        IRepository<T> CriarRepositorio();
    }
}
