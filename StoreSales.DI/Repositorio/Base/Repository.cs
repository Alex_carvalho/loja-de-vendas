﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace StoreSales.DI.Repositorio.Base
{
    public class Repository<TEntity, TDbContext> : IRepository<TEntity> 
        where TEntity : class
        where TDbContext : class
    {
        private enum RepositoryAction
        {
            Updating,
            Inserting,
            Deleting,
            Searching
        }

        private RepositoryAction _currentAction;

        private DbContext CreateContext()
        {
            return (DbContext)Activator.CreateInstance(typeof(TDbContext));
        }

        public virtual TEntity Save(TEntity entity)
        {
            return Save(entity, false);
        }

        public virtual TEntity Save(IEnumerable<TEntity> entities)
        {
            return Save(entities, false).FirstOrDefault();
        }

        public virtual TEntity Save(TEntity entity, bool saveNestedProperties)
        {
            return Save(new TEntity[] { entity }, saveNestedProperties).FirstOrDefault();
        }

        public virtual IEnumerable<TEntity> Save(IEnumerable<TEntity> entities, bool saveNestedProperties)
        {
            var enumerable = entities as IList<TEntity> ?? entities.ToList();
            using (var context = CreateContext())
            {
                foreach (var entity in enumerable)
                {
                    int primaryKeyValue = IdentityPrimaryKeyValue(entity, context);

                    _currentAction = primaryKeyValue == 0 ? RepositoryAction.Inserting : RepositoryAction.Updating;

                    SaveEntity(entity, context, primaryKeyValue);

                    UpdateNestedProperties(entity, context, saveNestedProperties);
                }

                context.SaveChanges();
            }

            return enumerable;
        }

        private void UpdateNestedProperties(TEntity entity, DbContext context, bool saveNestedProperties)
        {
            Type typeGeneric = entity.GetType();

            foreach (var property in typeGeneric.GetProperties())
            {
                if (IsEntityProperty(property))
                {
                    // Aqui eu identifico se uma entidade é um tipo complexo...

                    if (IsArray(entity, property))
                    {
                        var itensOfArray = (IEnumerable<object>)property.GetValue(entity);

                        if (itensOfArray != null)
                        {
                            foreach (var itemOfArray in itensOfArray)
                            {
                                SetStateOfEntity(itemOfArray, context, saveNestedProperties, entity);
                            }
                        }
                    }
                    else
                    {
                        var nestedPropertyToUpdate = property.GetValue(entity);

                        if (nestedPropertyToUpdate != null)
                        {
                            SetStateOfEntity(nestedPropertyToUpdate, context, saveNestedProperties, entity);
                        }
                    }
                }
            }
        }

        private bool IsArray(object entity, PropertyInfo property)
        {
            bool returnValue = (property.PropertyType.IsArray ||
                                property.GetValue(entity, null) is IEnumerable<object>)
                               && property.PropertyType != typeof(byte[]);

            return returnValue;
        }

        private bool IsEntityProperty(PropertyInfo property)
        {
            return property.PropertyType.IsClass &&
                   property.PropertyType != typeof(string) &&
                   property.PropertyType != typeof(byte[]);
        }

        private void SetNestedPropertiesStatus(object entity, DbContext context, EntityState state, object parent)
        {
            Type typeGeneric = entity.GetType();

            foreach (var property in typeGeneric.GetProperties())
            {
                if (IsEntityProperty(property))
                {
                    if (property.PropertyType.IsArray || property.GetValue(entity, null) is IEnumerable<object>)
                    {
                        var itensOfArray = (IEnumerable<object>)property.GetValue(entity, null);

                        if (itensOfArray != null)
                        {
                            foreach (object itemOfArray in itensOfArray)
                            {
                                int nestedPropertyPrimaryKeyValue = IdentityPrimaryKeyValue(itemOfArray, context);

                                if (itemOfArray != parent)
                                {
                                    var currentStatus = context.Entry(itemOfArray).State;

                                    if (!(nestedPropertyPrimaryKeyValue == 0 && currentStatus == EntityState.Added))
                                    {
                                        context.Entry(itemOfArray).State = state;
                                        SetNestedPropertiesStatus(itemOfArray, context, state, entity);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        var nestedPropertyToUpdate = property.GetValue(entity, null);

                        if (nestedPropertyToUpdate != null)
                        {
                            if (nestedPropertyToUpdate != parent)
                            {
                                context.Entry(nestedPropertyToUpdate).State = state;

                                SetNestedPropertiesStatus(nestedPropertyToUpdate, context, state, entity);
                            }
                        }
                    }
                }
            }
        }

        private void SetStateOfEntity(object entity, DbContext context, bool saveNestedProperties, object parent)
        {
            var targetState = EntityState.Added;

            int nestedPropertyPrimaryKeyValue = IdentityPrimaryKeyValue(entity, context);

            if (_currentAction == RepositoryAction.Inserting)
            {
                if (nestedPropertyPrimaryKeyValue == 0)
                    targetState = saveNestedProperties ? EntityState.Added : EntityState.Unchanged;
                else
                    targetState = EntityState.Unchanged;
            }
            
            else if (_currentAction == RepositoryAction.Updating)
            {
                if (nestedPropertyPrimaryKeyValue == 0)
                    targetState = saveNestedProperties ? EntityState.Added : EntityState.Unchanged;
                else
                    targetState = saveNestedProperties ? EntityState.Modified : EntityState.Unchanged;
            }

            context.Entry(entity).State = targetState;

            if (targetState == EntityState.Unchanged)
                SetNestedPropertiesStatus(entity, context, targetState, parent);
        }

        private void SaveEntity(TEntity entity, DbContext context, int currentPrimaryKeyValue)
        {
            if (currentPrimaryKeyValue == 0)
                context.Set<TEntity>().Add(entity);
            else
                context.Entry(entity).State = EntityState.Modified;
        }

        private int IdentityPrimaryKeyValue(object entity, DbContext context)
        {
            int returnValue = 0;
            var primaryKeyProperties = GetKeyNames(context);

            Type typeGeneric = entity.GetType();

            var primaryKeyProperty = typeGeneric.GetProperties().Where(p => string.Compare(p.Name, primaryKeyProperties[0], true) == 0).FirstOrDefault();

            if (primaryKeyProperty != null)
            {
                returnValue = (int)primaryKeyProperty.GetValue(entity);
            }

            return returnValue;
        }

        private string[] GetKeyNames(DbContext context)
        {
            ObjectContext objectContext = ((IObjectContextAdapter)context).ObjectContext;
            ObjectSet<TEntity> set = objectContext.CreateObjectSet<TEntity>();
            IEnumerable<string> keyNames = set.EntitySet.ElementType
                                                        .KeyMembers
                                                        .Select(k => k.Name);

            return keyNames.ToArray();
        }

        public void Delete(TEntity entity)
        {
            _currentAction = RepositoryAction.Deleting;

            using (var context = CreateContext())
            {
                context.Set<TEntity>().Attach(entity);
                context.Set<TEntity>().Remove(entity);

                context.SaveChanges();
            }
        }

        public IEnumerable<TEntity> Query(Expression<Func<TEntity, bool>> query)
        {
            _currentAction = RepositoryAction.Searching;

            return Query(query, null);
        }

        public IEnumerable<TEntity> GetAll(params string[] includeProperties)
        {
            _currentAction = RepositoryAction.Searching;

            IEnumerable<TEntity> result;

            using (var context = CreateContext())
            {
                var dbSet = context.Set<TEntity>();

                DbQuery<TEntity> currentQuery = null;

                if (includeProperties != null && includeProperties.Count() > 0)
                {
                    foreach (string propertyInclude in includeProperties)
                    {
                        if (currentQuery == null)
                            currentQuery = dbSet.Include(propertyInclude);
                        else
                            currentQuery.Include(propertyInclude);
                    }

                    result = currentQuery;
                }
                else
                {
                    result = dbSet;
                }

                if (result != null) 
                    result = result.ToList();
            }

            return result;
        }

        public IEnumerable<TEntity> Query(Expression<Func<TEntity, bool>> query, params string[] includeProperties)
        {
            _currentAction = RepositoryAction.Searching;

            IEnumerable<TEntity> result;

            using (var context = CreateContext())
            {
                var dbSet = context.Set<TEntity>();

                DbQuery<TEntity> currentQuery = null;

                if (includeProperties != null && includeProperties.Count() > 0)
                {
                    foreach (string propertyInclude in includeProperties)
                    {
                        if (currentQuery == null)
                            currentQuery = dbSet.Include(propertyInclude);
                        else
                            currentQuery.Include(propertyInclude);
                    }
                }

                result = currentQuery == null ? dbSet.Where(query) : currentQuery.Where(query);

                result = result.ToList();
            }

            return result;
        }
    }
}
