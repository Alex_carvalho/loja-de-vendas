﻿using StoreSales.DI.DataContext;
using StoreSales.DI.Repositorio.Base;

namespace StoreSales.DI.Repositorio
{
    public class RepositoryFactory<T> : IRepositoryFactory<T> where T : class
    {
        public IRepository<T> CriarRepositorio()
        {
            return new Repository<T, DataContexto>();
        }
    }
}
