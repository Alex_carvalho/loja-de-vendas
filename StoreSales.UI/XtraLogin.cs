﻿using System;
using System.Windows.Forms;
using StoreSales.DI.Factory;

namespace StoreSales.UI
{
    public partial class XtraLogin : DevExpress.XtraEditors.XtraForm
    {
        public XtraLogin()
        {
            InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                var usuario = new UsuariosFactory();

                MessageBox.Show(usuario.VerificaLogin(txtUser.Text, txtPass.Text) != null
                    ? @"Usuario ok"
                    : @"Usuario ou Senha inválidos.");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void brnSair_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void XtraLogin_Load(object sender, EventArgs e)
        {
            var usuario = new UsuariosFactory();

            if (usuario.RetornaSeExisteUsuario().Count == 0)
            {
                MessageBox.Show(
                    @"No momento não existe nenhum registro de usuário no banco de dados.\n É necessário cadastrar um para entrar no sistema, clique em ok para abrir o formulário para cadastra-lo.",
                    @"Informação", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}