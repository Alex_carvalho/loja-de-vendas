﻿using System;
using DevExpress.XtraSplashScreen;

namespace StoreSales.UI
{
    public partial class XtraSplashScreen : SplashScreen
    {
        public XtraSplashScreen()
        {
            InitializeComponent();
        }

        #region Overrides

        public override void ProcessCommand(Enum cmd, object arg)
        {
            base.ProcessCommand(cmd, arg);
        }

        #endregion

        public enum SplashScreenCommand
        {
        }
    }
}